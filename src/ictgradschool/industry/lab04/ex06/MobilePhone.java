package ictgradschool.industry.lab04.ex06;

import com.sun.xml.internal.bind.v2.TODO;

public class MobilePhone {

    private String brand;
    private String model;
    private double price;

    
    public MobilePhone(String brand, String model, double price) {
        this.brand = brand;
        this.model = model;
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }
    
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return (brand + " " + model + " which cost $" +price);
    }

    public boolean isCheaperThan(MobilePhone phone) {
        return false;
    }


    public boolean equals(MobilePhone phone) {
        return (price == phone.getPrice()) && (model.equals(phone.getModel())) && (brand.equals(phone.getBrand()));
    }
}


