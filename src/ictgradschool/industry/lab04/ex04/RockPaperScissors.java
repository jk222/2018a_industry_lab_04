package ictgradschool.industry.lab04.ex04;

import ictgradschool.Keyboard;

/**
 * A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    public static final int SCISSORS = 2;
    public static final int PAPER = 3;
    public static final int QUIT = 4;


    public void start() {

        System.out.println("Hi! What is your name?");
        String usersName = Keyboard.readInput();


        System.out.println("1. Rock");
        System.out.println("2. Scissors");
        System.out.println("3. Paper");
        System.out.println("4. Quit");
        System.out.println("");
        System.out.println("Enter your choice:");
        String usersNumber = Keyboard.readInput();
        int usersEntryNumber = Integer.parseInt(usersNumber);

        if (usersEntryNumber == 4){
            System.out.println("Goodbye " + usersName + ". Thanks for playing.");
            return;
        }
        else{

            int computerChoice = (int) (Math.random() * 3) + 1;

            displayPlayerChoice(usersName, usersEntryNumber);
            displayPlayerChoice("Computer", computerChoice);


            boolean userWinsGame = userWins(usersEntryNumber, computerChoice);

            String resultString = getResultString(usersEntryNumber, computerChoice);

            if (usersEntryNumber == computerChoice) {
                // tie because ...
                System.out.println("No one wins");
            } else if (userWinsGame == true) {
                System.out.println(usersName + " wins because " + resultString);
            } else {
                System.out.println("The computer wins because " + resultString);
            }
        }

    }

    public void displayPlayerChoice(String name, int choice) {

        System.out.print(name + " chose ");
        switch (choice) {
            case ROCK:
                System.out.print("Rock");
                break;
            case SCISSORS:
                System.out.print("Scissors");
                break;
            case PAPER:
                System.out.print("Paper");
                break;
            default:
                System.out.print("an invalid option");
                break;
        }
        System.out.println(".");

    }

    public boolean userWins(int playerChoice, int computerChoice) {

        if (playerChoice == 1 && computerChoice == 2) {
            return true;
        } else if (playerChoice == 1 && computerChoice == 3) {
            return false;
        } else if (playerChoice == 2 && computerChoice == 3) {
            return true;
        } else if (playerChoice == 2 && computerChoice == 1) {
            return false;
        } else if (playerChoice == 3 && computerChoice == 1) {
            return true;
        } else if (playerChoice == 3 && computerChoice == 2) {
            return false;
        } else {
            return false;
        }



    }

    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = " you chose the same as the computer";


        if (playerChoice == 1 && computerChoice == 2) {
            return ROCK_WINS;
        } else if (playerChoice == 1 && computerChoice == 3) {
            return PAPER_WINS;
        } else if (playerChoice == 2 && computerChoice == 3) {
            return SCISSORS_WINS;
        } else if (playerChoice == 2 && computerChoice == 1) {
            return ROCK_WINS;
        } else if (playerChoice == 3 && computerChoice == 1) {
            return PAPER_WINS;
        } else if (playerChoice == 3 && computerChoice == 2) {
            return SCISSORS_WINS;
        } else if (playerChoice == computerChoice) {
            return TIE;
        } else {
            return null;
        }

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
