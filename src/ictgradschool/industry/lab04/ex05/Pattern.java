package ictgradschool.industry.lab04.ex05;

public class Pattern {

    private int quantitySymbols;
    private char characterSymbol;

    public Pattern(int quantitySymbol, char characterSymbol){
        this.quantitySymbols = quantitySymbol;
        this.characterSymbol = characterSymbol;
    }

    public int getNumberOfCharacters(){
        return quantitySymbols;

    }

    public void setNumberOfCharacters(int quantitySymbol){
        this.quantitySymbols = quantitySymbol;

    }

    @Override
    public String toString() {
        String sequence = "";

        for (int i = 0; i< quantitySymbols; i++){
            sequence+=characterSymbol;
        }

        return sequence;
    }
}
