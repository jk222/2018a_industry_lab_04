package ictgradschool.industry.lab04.ex03;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    public void start() {

        // TODO Write your code here.
        int goal = randomNumberGenerator();
        int guess = 0;

        while (guess != goal){
            guess = userEntryGuess();
            if (guess>goal){
                System.out.println("Too high, try again");
            }
            else if(guess<goal){
                System.out.println("Too low, try again");
            }
            else   {
                System.out.println("Perfect!");
                System.out.println("Goodbye");
            }

        }
    }

    private int randomNumberGenerator (){
        int randomNumber = (int)(Math.random() * 100) + 1;
        return randomNumber;
    }

    private int userEntryGuess(){
        System.out.println("Enter your guess (1-100): ");
        String usersGuessNumber = Keyboard.readInput();
        int usersGuess = Integer.parseInt(usersGuessNumber);
        return usersGuess;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
